﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TileMap))]
public class TileMapMouse : MonoBehaviour
{
    Collider _collider;
    Renderer _renderer;
    TileMap _tileMap;
    Vector3 curerntTileCoord;
    public Transform selectionCube;

    /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        _collider = GetComponent<Collider>();
        _renderer = GetComponent<Renderer>();
        _tileMap = GetComponent<TileMap>();
    }
    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (_collider.Raycast(ray, out hitInfo, Mathf.Infinity))
        {
            int x = Mathf.FloorToInt(hitInfo.point.x / _tileMap.tileSize);
            int z = Mathf.FloorToInt(hitInfo.point.z / _tileMap.tileSize);

            curerntTileCoord.x = x;
            curerntTileCoord.z = z;

            selectionCube.transform.position = curerntTileCoord;
        }
        else
        {
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("click");
        }
    }
}
